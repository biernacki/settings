set nocompatible              " be iMproved, required
filetype off                  " required
set nocompatible
set rtp+=$HOME/.vim/bundle/vundle
let path='$HOME/.vim/bundle'
call vundle#rc('$HOME/.vim/bundle')
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'scrooloose/nerdcommenter'
Plugin 'bling/vim-airline'
Plugin 'orenhe/pylint.vim'
Plugin 'tpope/vim-fugitive'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" " To ignore plugin indent changes, instead use:
" "filetype plugin on
" "
" " Brief help
" " :PluginList       - lists configured plugins
" " :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" " :PluginSearch foo - searches for foo; append `!` to refresh local cache
" " :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
" "
" " see :h vundle for more details or wiki for FAQ
" " Put your non-Plugin stuff after this line

"=================================================
" My settings
"=================================================

:set mouse=a        " mouse on click
set shiftwidth=4
set tabstop=4
set softtabstop=4
set smarttab
set expandtab
syntax on           " syntax highlighting on
set laststatus=2
set hlsearch        " highlights search
set relativenumber  " sets relative numbers
