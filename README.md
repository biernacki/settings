### This repository contains some useful settings for various linux/unix programs. Make sure to put them in the right place and prepend '.' if needed

Settings for:

* bash
* vim
* matplotlib


Use at your own responsibility and feel free to be inspired/contribute
